# CS:GO Notes

## Description

Contains personal notes of Pavel related to setting up CS:GO.

## Good info
- [TheWarOwl @YouTube](https://www.youtube.com/user/TheWarOwl/playlists)


## Known issues 

### Win
- N/A
  
### MacOS
- [Mic not working [MacOS Catalina] #2201](https://github.com/ValveSoftware/csgo-osx-linux/issues/2201)

## Using custom config

### More Info

- [CS:GO - Setting up your game for the first time (2019 Approved)
](https://www.youtube.com/watch?v=uP61kaMHiE0)
- CS:GO - Pro Crosshairs [Teams](https://csgocrosshairs.com/teams/) & [Generator](https://csgocrosshairs.com/generator)
- [Pro Settings](https://prosettings.net/counterstrike/) (e.g. [Astralis](https://prosettings.net/counterstrike/astralis/), [EG](https://prosettings.net/counterstrike/evil-geniuses/) etc. )
- [Finding Your Sensitivity and Mouse Settings by TheWarOwl](https://www.youtube.com/watch?v=ct8U06Wonhk)

### How configure
1. Go to Steam UI -> CS:GO -> Properties -> Launch Options and set "-novid -console -tickrate 128 +exec my.cfg"
   ![Set Launch Options - CS:GO](01-Set-Start-Params.png)

2. Review [my.cfg](my.cfg) and make sure it fits your preference.
   
3. Copy my.cfg to cfg folder
   
   - win:

     `copy my.cfg C:\Program Files (x86)\Steam\userdata\<your-account-id>\730\local\cfg`
   
   - macos: 
   
     `cp my.cfg ~/Library/Application\ Support/Steam/steamapps/common/Counter-Strike\ Global\ Offensive/csgo/cfg)`
